# Myanimelist Dataset : Creation & Analysis

The dataset was created by importing myanimelist.net data and cleaning it using Python.

## The project consists of:
- {+ Script that will create an anime database from the database of the myanimelist.net website (over 19500 animes).  +}

- {+To clean this data and analyze it, a second script will be used.  +}

## About the project

This project will assist in the development of a machine learning module that will be able to recommend anime to the user based on this dataset.

## This project uses the following modules :

- Sending an HTTP request to the website using `requests`.

- Scraping the web with `Beautiful Soup`.

- A directory to store data will be created using `OS` and `pathlib` modules.

- Creating json file using the `json` module.

## An example of an extracted part from the created JSON file that contains all animes information created by using the script

```json
{
  "title": "Haikyuu!!",
  "synopsis": "Inspired after watching a volleyball ace nicknamed \"Little Giant\" in action, small-statured Shouyou Hinata revives the volleyball club at his middle school. The newly-formed team even makes it to a tournament; however, their first match turns out to be their last when they are brutally squashed by the \"King of the Court,\" Tobio Kageyama. Hinata vows to surpass Kageyama, and so after graduating from middle school, he joins Karasuno High School's volleyball team—only to find that his sworn rival, Kageyama, is now his teammate.Thanks to his short height, Hinata struggles to find his role on the team, even with his superior jumping power. Surprisingly, Kageyama has his own problems that only Hinata can help with, and learning to work together appears to be the only way for the team to be successful. Based on Haruichi Furudate's popular shounen manga of the same name, Haikyuu!! is an exhilarating and emotional sports comedy following two determined athletes as they attempt to patch a heated rivalry in order to make their high school volleyball team the best in Japan.",
  "poster_link": "https://cdn.myanimelist.net/images/anime/7/76014.jpg",
  "Type": "TV",
  "Episodes": "25",
  "Status": "Finished Airing",
  "Aired": "Apr 6, 2014 to Sep 21, 2014",
  "Premiered": "Spring 2014",
  "Broadcast": "Sundays at 17:00 (JST)",
  "Producers": "Dentsu,       Mainichi Broadcasting System,       Movic,       TOHO animation,       Shueisha,       Spacey Music Entertainment",
  "Licensors": "Sentai Filmworks",
  "Studios": "Production I.G",
  "Source": "Manga",
  "Genres": "ComedyComedy,         DramaDrama,         SportsSports",
  "Theme": "SchoolSchool",
  "Demographic": "ShounenShounen",
  "Duration": "24 min. per ep.",
  "Rating": "PG-13 - Teens 13 or older",
  "Score": "8.461 (scored by 938982938,982 users)",
  "Ranked": "#1272",
  "Popularity": "#37",
  "Members": "1,552,137",
  "Favorites": "61,391"
}
```











 




